import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt


def find_location(my_lat_value, my_lon_value, dataset):
  
    xlat_variable = dataset.variables['XLAT'][0] 
    xlon_variable = dataset.variables['XLONG'][0] 

    distances_from_point_to_each_gridpoint = ((xlat_variable - my_lat_value) ** 2 +
                                              (xlon_variable - my_lon_value) ** 2) ** 0.5
    min_distance = distances_from_point_to_each_gridpoint.min()

    index = np.where(distances_from_point_to_each_gridpoint == min_distance)
   
    return index[0][0], index[1][0]

point = (42.4440, -76.5019)

dataset = Dataset("wrfout.nc")

index = find_location(point[0], point[1], dataset)
print('index is:', index)


t2_variable = dataset.variables['T2'] 

time_labels_variable = dataset.variables['Times']

count_of_observations = len(t2_variable)

x_axis_labels = []
temperature_values = []

for n in range(0, count_of_observations):
    x_axis_labels.append(time_labels_variable[n].tostring().decode())
    temperature_values.append(t2_variable[n][index])

plt.xticks(range(0, count_of_observations), x_axis_labels, rotation=90)
plt.plot(range(0, count_of_observations), temperature_values)
plt.tight_layout() 
plt.show()